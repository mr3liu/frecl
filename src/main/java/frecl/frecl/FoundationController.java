package frecl.frecl;

import javafx.beans.property.ObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.scene.layout.Pane;

public class FoundationController extends Pane {

    private final Foundation foundation;

    public FoundationController(Foundation foundation, ObjectProperty<Card> selectedCard) {
        this.foundation = foundation;

        init(selectedCard);
    }

    private void init(ObjectProperty<Card> selectedCard) {
        getChildren().add(new CardController(selectedCard));
        foundation.getCards().addListener((ListChangeListener<Card>) c -> {
            getChildren().remove(0);
            getChildren().add(new CardController(foundation.getLast(), selectedCard));
        });

    }
}
