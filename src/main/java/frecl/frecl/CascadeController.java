package frecl.frecl;

import javafx.beans.property.ObjectProperty;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;

import java.io.IOException;

import static frecl.frecl.CardController.WIDTH;

public class CascadeController extends VBox {

    private final Cascade cascade;

    public CascadeController(Cascade cascade, ObjectProperty<Card> selectedCard) {
        this.cascade = cascade;

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("cascade-view.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        init(selectedCard);
    }

    private void init(ObjectProperty<Card> selectedCard) {
        setMinWidth(WIDTH);
        setMaxWidth(WIDTH);
        for (int i = 0; i < cascade.size(); ++i)
            getChildren().add(new CardController(cascade.get(i), selectedCard));
        cascade.getCards().addListener((ListChangeListener<Card>) c -> {
            getChildren().clear();
            for (int i = 0; i < cascade.size(); ++i)
                getChildren().add(new CardController(cascade.get(i), selectedCard));
        });
    }
}
