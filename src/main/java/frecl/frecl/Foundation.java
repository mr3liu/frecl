package frecl.frecl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;

public class Foundation {

    private final ObservableList<Card> cards = FXCollections.observableList(new ArrayList<>());

    public void add(Card card) {
        cards.add(card);
    }

    public Card removeLast() {
        return cards.remove(cards.size() - 1);
    }

    public Card getLast() {
        return cards.size() == 0 ? null : cards.get(cards.size() - 1);
    }

    public ObservableList<Card> getCards() {
        return cards;
    }
}
