package frecl.frecl;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class Game {

    private final Deal deal;
    private final List<Cell> cells = new ArrayList<>();
    private final List<Foundation> foundations = new ArrayList<>();
    private final List<Cascade> cascades = new ArrayList<>();
    private Selectable selected;
    private final ObjectProperty<Card> selectedCard = new SimpleObjectProperty<>(null);
    private String selectHistory = "";

    public Game(Deal deal) {
        this.deal = deal;
        for (int i = 0; i < 4; ++i) cells.add(new Cell());
        for (int i = 0; i < 4; ++i) foundations.add(new Foundation());
        for (int i = 0; i < 8; ++i) cascades.add(new Cascade());
        for (int i = 0; i < 52; ++i) {
            cascades.get(i % 8).add(deal.dealNextCard());
        }
    }

    public Deal getDeal() {
        return deal;
    }

    public List<Cell> getCells() {
        return cells;
    }

    public List<Foundation> getFoundations() {
        return foundations;
    }

    public List<Cascade> getCascades() {
        return cascades;
    }

    public ObjectProperty<Card> getSelectedCard() {
        return selectedCard;
    }

    public String getSelectHistory() {
        return selectHistory;
    }

    public void select(Selectable newSelected) {
        if (newSelected == null) {
            selected = null;
            selectedCard.set(null);
            return;
        }
        if (selected == null) {
            Card card = null;
            switch (newSelected.getType()) {
                case CELL -> card = cells.get(newSelected.getIndex()).get();
                case FOUNDATION -> card = foundations.get(newSelected.getIndex()).getLast();
                case CASCADE -> card = cascades.get(newSelected.getIndex()).getLast();
            }
            if (card != null) {
                selected = newSelected;
                selectedCard.set(card);
            }
        } else {
            switch (selected.getType()) {
                case CELL -> {
                    switch (newSelected.getType()) {
                        case CELL_CASCADE_SPECIAL -> {
                            for (Selectable selectable: List.of(
                                    Selectable.CASCADE_1, Selectable.CASCADE_2, Selectable.CASCADE_3, Selectable.CASCADE_4,
                                    Selectable.CASCADE_5, Selectable.CASCADE_6, Selectable.CASCADE_7, Selectable.CASCADE_8
                            )) {
                                if (cascades.get(selectable.getIndex()).getLast() == null) {
                                    select(selectable);
                                    return;
                                }
                            }
                        }
                        case FOUNDATION_SPECIAL -> {
                            for (Selectable selectable: List.of(
                                    Selectable.FOUNDATION_1, Selectable.FOUNDATION_2, Selectable.FOUNDATION_3, Selectable.FOUNDATION_4
                            )) {
                                if (cells.get(selected.getIndex()).get().canBePutOnFoundation(foundations.get(selectable.getIndex()).getLast())) {
                                    select(selectable);
                                    return;
                                }
                            }
                        }
                        case CELL -> {
                            Cell newCell = cells.get(newSelected.getIndex());
                            if (newCell.get() == null) {
                                Cell cell = cells.get(selected.getIndex());
                                newCell.add(cell.remove());
                                selectHistory += selected.getKey() + newSelected.getKey();
                                selected = null;
                                selectedCard.set(null);
                            } else {
                                selected = newSelected;
                                selectedCard.set(newCell.get());
                            }
                        }
                        case CASCADE -> {
                            Cascade newCascade = cascades.get(newSelected.getIndex());
                            Cell cell = cells.get(selected.getIndex());
                            if (cell.get().canBePutOnCascade(newCascade.getLast())) {
                                newCascade.add(cell.remove());
                                selectHistory += selected.getKey() + newSelected.getKey();
                                selected = null;
                                selectedCard.set(null);
                            } else {
                                selected = newSelected;
                                selectedCard.set(newCascade.getLast());
                            }
                        }
                        case FOUNDATION -> {
                            Foundation newFoundation = foundations.get(newSelected.getIndex());
                            Cell cell = cells.get(selected.getIndex());
                            if (cell.get().canBePutOnFoundation(newFoundation.getLast())) {
                                newFoundation.add(cell.remove());
                                selectHistory += selected.getKey() + newSelected.getKey();
                                selected = null;
                                selectedCard.set(null);
                                autoMoveToFoundations();
                            } else if (newFoundation.getLast() != null) {
                                selected = newSelected;
                                selectedCard.set(newFoundation.getLast());
                            }
                        }
                    }
                }
                case FOUNDATION -> {
                    switch (newSelected.getType()) {
                        case CELL -> {
                            Cell newCell = cells.get(newSelected.getIndex());
                            if (newCell.get() == null) {
                                Foundation foundation = foundations.get(selected.getIndex());
                                newCell.add(foundation.removeLast());
                                selectHistory += selected.getKey() + newSelected.getKey();
                                selected = null;
                                selectedCard.set(null);
                            } else {
                                selected = newSelected;
                                selectedCard.set(newCell.get());
                            }
                        }
                        case CASCADE -> {
                            Cascade newCascade = cascades.get(newSelected.getIndex());
                            Foundation foundation = foundations.get(selected.getIndex());
                            if (foundation.getLast().canBePutOnCascade(newCascade.getLast())) {
                                newCascade.add(foundation.removeLast());
                                selectHistory += selected.getKey() + newSelected.getKey();
                                selected = null;
                                selectedCard.set(null);
                            } else {
                                selected = newSelected;
                                selectedCard.set(newCascade.getLast());
                            }
                        }
                        case FOUNDATION -> {
                            Foundation newFoundation = foundations.get(newSelected.getIndex());
                            Foundation foundation = foundations.get(selected.getIndex());
                            if (foundation.getLast().canBePutOnFoundation(newFoundation.getLast())) {
                                newFoundation.add(foundation.removeLast());
                                selectHistory += selected.getKey() + newSelected.getKey();
                                selected = null;
                                selectedCard.set(null);
                            } else if (newFoundation.getLast() != null) {
                                selected = newSelected;
                                selectedCard.set(newFoundation.getLast());
                            }
                        }
                    }
                }
                case CASCADE -> {
                    switch (newSelected.getType()) {
                        case CELL_CASCADE_SPECIAL -> {
                            for (Selectable selectable: List.of(
                                    Selectable.CELL_1, Selectable.CELL_2, Selectable.CELL_3, Selectable.CELL_4
                            )) {
                                if (cells.get(selectable.getIndex()).get() == null) {
                                    select(selectable);
                                    return;
                                }
                            }
                            for (Selectable selectable: List.of(
                                    Selectable.CASCADE_1, Selectable.CASCADE_2, Selectable.CASCADE_3, Selectable.CASCADE_4,
                                    Selectable.CASCADE_5, Selectable.CASCADE_6, Selectable.CASCADE_7, Selectable.CASCADE_8
                            )) {
                                if (cascades.get(selectable.getIndex()).getLast() == null) {
                                    select(selectable);
                                    return;
                                }
                            }
                        }
                        case FOUNDATION_SPECIAL -> {
                            for (Selectable selectable: List.of(
                                    Selectable.FOUNDATION_1, Selectable.FOUNDATION_2, Selectable.FOUNDATION_3, Selectable.FOUNDATION_4
                            )) {
                                if (cascades.get(selected.getIndex()).getLast().canBePutOnFoundation(foundations.get(selectable.getIndex()).getLast())) {
                                    select(selectable);
                                    return;
                                }
                            }
                        }
                        case CELL -> {
                            Cell newCell = cells.get(newSelected.getIndex());
                            if (newCell.get() == null) {
                                Cascade cascade = cascades.get(selected.getIndex());
                                newCell.add(cascade.removeLast());
                                selectHistory += selected.getKey() + newSelected.getKey();
                                selected = null;
                                selectedCard.set(null);
                                autoMoveToFoundations();
                            } else {
                                selected = newSelected;
                                selectedCard.set(newCell.get());
                            }
                        }
                        case CASCADE -> {
                            Cascade newCascade = cascades.get(newSelected.getIndex());
                            Cascade cascade = cascades.get(selected.getIndex());
                            /* Supermove */ // not rigorously tested
                            boolean movingToFreeCascade = newCascade.getLast() == null;
                            int cascadeSize = cascade.size();
                            int maxSupermoveSize = movingToFreeCascade
                                    ? (int) ((1 + getNumFreeCells()) * Math.pow(2, getNumFreeCascades() - 1))
                                    : (int) ((1 + getNumFreeCells()) * Math.pow(2, getNumFreeCascades()));
                            if (movingToFreeCascade) {
                                int i;
                                for (i = 1; i <= Math.min(cascadeSize, maxSupermoveSize); ++i) {
                                    if (cascadeSize - i - 1 < 0 ||
                                            !cascade.get(cascadeSize - i).canBePutOnCascade(cascade.get(cascadeSize - i - 1))) {
                                        ++i;
                                        break;
                                    }
                                }
                                --i; /* Counteract final ++i */
                                for (int j = 0; j < i; ++j) {
                                    newCascade.add(cascade.remove(cascadeSize - i));
                                }
                                selectHistory += selected.getKey() + newSelected.getKey();
                                selected = null;
                                selectedCard.set(null);
                                autoMoveToFoundations();
                            } else {
                                for (int i = 1; i <= Math.min(cascadeSize, maxSupermoveSize); ++i) {
                                    if (cascade.get(cascadeSize - i).canBePutOnCascade(newCascade.getLast())) {
                                        for (int j = 0; j < i; ++j) {
                                            newCascade.add(cascade.remove(cascadeSize - i));
                                        }
                                        selectHistory += selected.getKey() + newSelected.getKey();
                                        selected = null;
                                        selectedCard.set(null);
                                        autoMoveToFoundations();
                                        return;
                                    } else if (cascadeSize - i - 1 < 0 ||
                                            !cascade.get(cascadeSize - i).canBePutOnCascade(cascade.get(cascadeSize - i - 1))) {
                                        break;
                                    }
                                }
                                selected = newSelected;
                                selectedCard.set(newCascade.getLast());
                            }
                        }
                        case FOUNDATION -> {
                            Foundation newFoundation = foundations.get(newSelected.getIndex());
                            Cascade cascade = cascades.get(selected.getIndex());
                            if (cascade.getLast().canBePutOnFoundation(newFoundation.getLast())) {
                                newFoundation.add(cascade.removeLast());
                                selectHistory += selected.getKey() + newSelected.getKey();
                                selected = null;
                                selectedCard.set(null);
                                autoMoveToFoundations();
                            } else if (newFoundation.getLast() != null) {
                                selected = newSelected;
                                selectedCard.set(newFoundation.getLast());
                            }
                        }
                    }
                }
            }
        }
    }

    private int getNumFreeCells() {
        return (int) cells.stream().filter(cell -> cell.get() == null).count();
    }

    private int getNumFreeCascades() {
        return (int) cascades.stream().filter(cascade -> cascade.size() == 0).count();
    }

    private void autoMoveToFoundations() {
        start:
        while (true) {
            for (Foundation foundation: foundations) {
                Card foundationLast = foundation.getLast();
                if (!(foundationLast == null || foundationLast.getRank() == Card.Rank.ACE)) { /* Remove this block for greedy */ // not rigorously tested
                    int satisfiedFoundationCount = 0;
                    for (Foundation otherFoundation: foundations) {
                        Card otherFoundationLast = otherFoundation.getLast();
                        if (otherFoundationLast != null && otherFoundationLast.isRed() != foundationLast.isRed() &&
                                otherFoundationLast.getRank().ordinal() >= foundationLast.getRank().ordinal())
                            ++satisfiedFoundationCount;
                    }
                    if (satisfiedFoundationCount < 2) continue;
                }
                for (Cell cell: cells) {
                    if (cell.get() != null && cell.get().canBePutOnFoundation(foundationLast)) {
                        foundation.add(cell.remove());
                        continue start;
                    }
                }
                for (Cascade cascade: cascades) {
                    if (cascade.getLast() != null && cascade.getLast().canBePutOnFoundation(foundationLast)) {
                        foundation.add(cascade.removeLast());
                        continue start;
                    }
                }
            }
            break;
        }
        if (getNumFreeCells() == 4 && getNumFreeCascades() == 8) {
            try {
                int msNum = deal.getMsNum();
                String log = msNum == 0 ? "" : "m" + msNum + "\t";
                log += deal.getFreclNum().toString() + "\t" + selectHistory + "\n";
                Files.writeString(Paths.get("completed_deals.txt"), log,
                        StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
