package frecl.frecl;

import javafx.fxml.FXML;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;

import java.math.BigInteger;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import static frecl.frecl.Deal.MAX_FRECL_NUM;

public class FreclController {
    public static final double WIDTH = 480.0;
    public static final double HEIGHT = 720.0;

    @FXML
    private HBox cells;

    @FXML
    private HBox foundations;

    @FXML
    private HBox cascades;

    private Game game;

    protected void init() {
        /* Empty dummies */
        for (int i = 0; i < 4; ++i)
            cells.getChildren().add(new CardController(null));
        for (int i = 0; i < 4; ++i)
            foundations.getChildren().add(new CardController(null));
    }

    private void newGame() {
        cells.getChildren().clear();
        foundations.getChildren().clear();
        cascades.getChildren().clear();
        for (Cell cell: game.getCells())
            cells.getChildren().add(new CellController(cell, game.getSelectedCard()));
        for (Foundation foundation: game.getFoundations())
            foundations.getChildren().add(new FoundationController(foundation, game.getSelectedCard()));
        for (Cascade cascade: game.getCascades())
            cascades.getChildren().add(new CascadeController(cascade, game.getSelectedCard()));
        cells.getChildren().get(0).setOnMouseClicked(e -> game.select(Selectable.CELL_1));
        cells.getChildren().get(1).setOnMouseClicked(e -> game.select(Selectable.CELL_2));
        cells.getChildren().get(2).setOnMouseClicked(e -> game.select(Selectable.CELL_3));
        cells.getChildren().get(3).setOnMouseClicked(e -> game.select(Selectable.CELL_4));
        foundations.getChildren().get(0).setOnMouseClicked(e -> game.select(Selectable.FOUNDATION_1));
        foundations.getChildren().get(1).setOnMouseClicked(e -> game.select(Selectable.FOUNDATION_2));
        foundations.getChildren().get(2).setOnMouseClicked(e -> game.select(Selectable.FOUNDATION_3));
        foundations.getChildren().get(3).setOnMouseClicked(e -> game.select(Selectable.FOUNDATION_4));
        cascades.getChildren().get(0).setOnMouseClicked(e -> {game.select(Selectable.CASCADE_1); if (e.getClickCount() == 2) game.select(Selectable.CELL_CASCADE_SPECIAL);});
        cascades.getChildren().get(1).setOnMouseClicked(e -> {game.select(Selectable.CASCADE_2); if (e.getClickCount() == 2) game.select(Selectable.CELL_CASCADE_SPECIAL);});
        cascades.getChildren().get(2).setOnMouseClicked(e -> {game.select(Selectable.CASCADE_3); if (e.getClickCount() == 2) game.select(Selectable.CELL_CASCADE_SPECIAL);});
        cascades.getChildren().get(3).setOnMouseClicked(e -> {game.select(Selectable.CASCADE_4); if (e.getClickCount() == 2) game.select(Selectable.CELL_CASCADE_SPECIAL);});
        cascades.getChildren().get(4).setOnMouseClicked(e -> {game.select(Selectable.CASCADE_5); if (e.getClickCount() == 2) game.select(Selectable.CELL_CASCADE_SPECIAL);});
        cascades.getChildren().get(5).setOnMouseClicked(e -> {game.select(Selectable.CASCADE_6); if (e.getClickCount() == 2) game.select(Selectable.CELL_CASCADE_SPECIAL);});
        cascades.getChildren().get(6).setOnMouseClicked(e -> {game.select(Selectable.CASCADE_7); if (e.getClickCount() == 2) game.select(Selectable.CELL_CASCADE_SPECIAL);});
        cascades.getChildren().get(7).setOnMouseClicked(e -> {game.select(Selectable.CASCADE_8); if (e.getClickCount() == 2) game.select(Selectable.CELL_CASCADE_SPECIAL);});
    }

    protected void onKeyPressed(KeyEvent keyEvent) {
        String key = keyEvent.getCode().getChar();
        if (key.equals("N")) {
            /* Random freclNum */
            BigInteger freclNum;
            do {
                freclNum = new BigInteger(MAX_FRECL_NUM.bitLength(), new Random());
            } while (freclNum.compareTo(MAX_FRECL_NUM) >= 0);
            freclNum = freclNum.add(BigInteger.ONE);
            if (keyEvent.isShiftDown()) {
                game = new Game(new Deal(freclNum));
                newGame();
                return;
            }
            TextInputDialog dialog = new TextInputDialog(freclNum.toString());
            dialog.setTitle("");
            dialog.setHeaderText("Input frecl deal number:                                                                              ");
            dialog.setGraphic(null);
            dialog.showAndWait().ifPresent(response -> {
                game = new Game(new Deal(new BigInteger(response)));
                newGame();
            });
        } else if (key.equals("M")) {
            /* Random msNum */
            int msNum = ThreadLocalRandom.current().nextInt(1, 1000001);
            if (keyEvent.isShiftDown()) {
                game = new Game(new Deal(msNum));
                newGame();
                return;
            }
            TextInputDialog dialog = new TextInputDialog(Integer.toString(msNum));
            dialog.setTitle("");
            dialog.setHeaderText("Input Microsoft deal number:");
            dialog.setGraphic(null);
            dialog.showAndWait().ifPresent(response -> {
                game = new Game(new Deal(Integer.parseInt(response)));
                newGame();
            });
        } else if (game != null) {
            switch (key) {
                case "X" -> {
                    game = new Game(new Deal(game.getDeal().getFreclNum()));
                    newGame();
                }
                case "Z" -> {
                    String selectHistory = game.getSelectHistory();
                    if (selectHistory.length() >= 2) {
                        game = new Game(new Deal(game.getDeal().getFreclNum()));
                        newGame();
                        selectKeypresses(selectHistory.substring(0, selectHistory.length() - 2));
                    }
                }
                case "/" -> {
                    TextInputDialog dialog = new TextInputDialog();
                    dialog.setTitle("");
                    dialog.setHeaderText("Input keypresses:");
                    dialog.setGraphic(null);
                    dialog.showAndWait().ifPresent(this::selectKeypresses);
                }
                default -> {
                    Selectable selectable = Selectable.fromKey(key);
                    game.select(selectable);
                }
            }
        }
    }

    private void selectKeypresses(String keypressString) {
        for (String key_: keypressString.toUpperCase().split("")) {
            Selectable selectable = Selectable.fromKey(key_);
            game.select(selectable);
        }
    }
}
