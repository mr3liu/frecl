package frecl.frecl;

import javafx.beans.property.ObjectProperty;
import javafx.scene.layout.Pane;

public class CellController extends Pane {

    private final Cell cell;

    public CellController(Cell cell, ObjectProperty<Card> selectedCard) {
        this.cell = cell;

        init(selectedCard);
    }

    private void init(ObjectProperty<Card> selectedCard) {
        getChildren().add(new CardController(selectedCard));
        cell.getCard().addListener((c, o, n) -> {
            getChildren().remove(0);
            getChildren().add(new CardController(cell.get(), selectedCard));
        });
    }
}
