package frecl.frecl;

import javafx.beans.property.ObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.io.IOException;

import static javafx.scene.paint.Color.RED;

public class CardController extends AnchorPane {

    public static final double WIDTH = 48.0;
    public static final double HEIGHT = 32.0;

    private Card card;

    @FXML
    private Text text;

    public CardController(ObjectProperty<Card> selectedCard) {
        loadFxml();

        init(selectedCard);
    }

    public CardController(Card card, ObjectProperty<Card> selectedCard) {
        this.card = card;

        loadFxml();

        init(selectedCard);
    }

    private void loadFxml() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("card-view.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void init(ObjectProperty<Card> selectedCard) {
        setMinWidth(WIDTH);
        setMaxWidth(WIDTH);
        setMinHeight(HEIGHT);
        setMaxHeight(HEIGHT);
        if (card != null) {
            text.setText(card.toPrettyString());
            getStyleClass().add("nonempty");
            if (card.isRed()) text.setFill(RED);
            selectedCard.addListener((c, o, n) -> {
                if (selectedCard.get() == card)
                    getStyleClass().add("selected");
                else
                    getStyleClass().remove("selected");
            });
        }
    }
}
