package frecl.frecl;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Deal {

    /* 52! */
    public static final BigInteger MAX_FRECL_NUM =
            new BigInteger("80658175170943878571660636856403766975289505440883277824000000000000");

    public static final Map<Integer, BigInteger> SPECIAL_MS_DEALS = Map.of(
            -1, new BigInteger("10569926013955558202755856709118982560279989751167383898393601"),
            -2, new BigInteger("4714798800089740477881878903818613796232776844111088263470205723584"),
            -3, new BigInteger("80658168828988418649149018380029972931891821767487752839470205723584"),
            -4, new BigInteger("80468161408179847067714935947054959242102729063522957480629233542464")
    );

    public static void main(String[] args) {
        String input = new Scanner(System.in).nextLine();
        List<Integer> deckIndices = Arrays.stream(input.split("\\D+")).map(Integer::parseInt).collect(Collectors.toList());
        System.out.println(computeFreclNum(deckIndices));
    }

    private static BigInteger computeFreclNum(List<Integer> deckIndices) {
        List<Integer> deck = IntStream.range(0, 52).boxed().collect(Collectors.toList());
        BigInteger freclNum = BigInteger.ZERO;
        BigInteger[] factorials = computeFactorials();
        for (int i = 51; i >= 0; --i) {
            int index = deck.indexOf(deckIndices.remove(0));
            deck.remove(index);
            freclNum = freclNum.add(factorials[i].multiply(BigInteger.valueOf(index)));
        }
        return freclNum.add(BigInteger.ONE);
    }

    private static BigInteger[] computeFactorials() {
        BigInteger[] factorials = new BigInteger[52];
        factorials[0] = BigInteger.ONE;
        for (int i = 1; i < 52; ++i)
            factorials[i] = factorials[i - 1].multiply(BigInteger.valueOf(i));
        return factorials;
    }

    private int msNum;
    private final BigInteger freclNum;

    private final List<Card> cards = new ArrayList<>();

    /* Source: http://www.solitairelaboratory.com/mshuffle.txt */
    public Deal(int msNum) {
        this.msNum = msNum;
        if (-4 <= msNum && msNum <= -1) {
            freclNum = SPECIAL_MS_DEALS.get(msNum);
            cards.addAll(new Deal(freclNum).cards);
            return;
        }
        int[] deck = IntStream.range(0, 52).toArray();
        MSVCLCG lcg = new MSVCLCG(msNum);
        for (int i = 52; i > 0; ) {
            int j = lcg.gen() % i;
            cards.add(new Card(deck[j]));
            deck[j] = deck[--i];
        }

        freclNum = computeFreclNum(cards.stream().map(Card::getDeckIndex).collect(Collectors.toList()));
    }

    /* 1 to MAX_FRECL_NUM */
    public Deal(BigInteger freclNum) {
        this.freclNum = freclNum;
        freclNum = freclNum.subtract(BigInteger.ONE);
        BigInteger[] factorials = computeFactorials();
        List<Integer> deck = IntStream.range(0, 52).boxed().collect(Collectors.toList());
        for (int i = 51; i >= 0; --i) {
            cards.add(new Card(deck.remove(freclNum.divide(factorials[i]).intValue())));
            freclNum = freclNum.remainder(factorials[i]);
        }
    }

    public int getMsNum() {
        return msNum;
    }
    public BigInteger getFreclNum() {
        return freclNum;
    }
    public String toAsciiString() {
        return cards.stream().map(Card::toAsciiString).collect(Collectors.joining(" "));
    }

    public Card dealNextCard() {
        return cards.remove(0);
    }

}
