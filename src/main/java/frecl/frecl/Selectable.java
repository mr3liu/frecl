package frecl.frecl;

public enum Selectable {

    CELL_CASCADE_SPECIAL(Type.CELL_CASCADE_SPECIAL, 0, " "),
    FOUNDATION_SPECIAL(Type.FOUNDATION_SPECIAL, 0, "\n"),
    FOUNDATION_SPECIAL_ALT(Type.FOUNDATION_SPECIAL, 0, "H"),
    CELL_1(Type.CELL, 0, "Q"),
    CELL_2(Type.CELL, 1, "W"),
    CELL_3(Type.CELL, 2, "E"),
    CELL_4(Type.CELL, 3, "R"),
    FOUNDATION_1(Type.FOUNDATION, 0, "U"),
    FOUNDATION_2(Type.FOUNDATION, 1, "I"),
    FOUNDATION_3(Type.FOUNDATION, 2, "O"),
    FOUNDATION_4(Type.FOUNDATION, 3, "P"),
    CASCADE_1(Type.CASCADE, 0, "A"),
    CASCADE_2(Type.CASCADE, 1, "S"),
    CASCADE_3(Type.CASCADE, 2, "D"),
    CASCADE_4(Type.CASCADE, 3, "F"),
    CASCADE_5(Type.CASCADE, 4, "J"),
    CASCADE_6(Type.CASCADE, 5, "K"),
    CASCADE_7(Type.CASCADE, 6, "L"),
    CASCADE_8(Type.CASCADE, 7, ";");

    public enum Type {
        CELL,
        CELL_CASCADE_SPECIAL,
        FOUNDATION_SPECIAL,
        FOUNDATION,
        CASCADE
    }

    private final Type type;
    private final int index;
    private final String key;

    Selectable(Type type, int index, String key) {
        this.type = type;
        this.index = index;
        this.key = key;
    }

    public Type getType() {
        return type;
    }
    public int getIndex() {
        return index;
    }
    public String getKey() {
        return key;
    }

    public static Selectable fromKey(String key) {
        for (Selectable selectable: values()) {
            if (selectable.key.equals(key))
                return selectable;
        }
        return null;
    }
}
