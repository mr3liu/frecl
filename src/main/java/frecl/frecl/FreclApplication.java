package frecl.frecl;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class FreclApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("frecl-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), FreclController.WIDTH, FreclController.HEIGHT);
        stage.setTitle("frecl");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("icon.png")));
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

        FreclController controller = fxmlLoader.getController();
        controller.init();

        scene.setOnKeyPressed(controller::onKeyPressed);
    }

    public static void main(String[] args) {
        launch();
    }
}
