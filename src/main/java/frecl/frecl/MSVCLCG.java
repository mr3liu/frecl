package frecl.frecl;

/* Microsoft Visual C++ Linear Congruential Generator */
public class MSVCLCG {

    private int state;

    public MSVCLCG(int seed) {
        state = seed;
    }

    public int gen() {
        state = (state * 214013 + 2531011) & ((1 << 31) - 1);
        return state >> 16;
    }

}
