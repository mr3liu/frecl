package frecl.frecl;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Cell {

    private final ObjectProperty<Card> card = new SimpleObjectProperty<>(null);

    public void add(Card card) {
        this.card.set(card);
    }

    public Card remove() {
        Card cardCopy = card.get();
        card.set(null);
        return cardCopy;
    }

    public Card get() {
        return card.get();
    }

    public ObjectProperty<Card> getCard() {
        return card;
    }
}
