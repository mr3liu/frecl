package frecl.frecl;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;

public class Cascade {

    private final ObservableList<Card> cards = FXCollections.observableList(new ArrayList<>());

    public void add(Card card) {
        cards.add(card);
    }

    public Card remove(int index) {
        return cards.remove(index);
    }

    public Card removeLast() {
        return cards.remove(cards.size() - 1);
    }

    public int size() {
        return cards.size();
    }

    public Card get(int index) {
        return cards.get(index);
    }

    public Card getLast() {
        return cards.size() == 0 ? null : cards.get(cards.size() - 1);
    }

    public ObservableList<Card> getCards() {
        return cards;
    }
}
