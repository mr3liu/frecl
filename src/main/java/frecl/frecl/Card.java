package frecl.frecl;

public class Card {

    public enum Rank {
        ACE("A"),
        TWO("2"),
        THREE("3"),
        FOUR("4"),
        FIVE("5"),
        SIX("6"),
        SEVEN("7"),
        EIGHT("8"),
        NINE("9"),
        TEN("T", "10"),
        JACK("J"),
        QUEEN("Q"),
        KING("K");

        private final String ascii;
        private final String pretty;

        Rank(String ascii) {
            this.ascii = ascii;
            this.pretty = ascii;
        }
        Rank(String ascii, String pretty) {
            this.ascii = ascii;
            this.pretty = pretty;
        }

        public String toAsciiString() {
            return ascii;
        }
        public String toPrettyString() {
            return pretty;
        }
    }

    public enum Suit {
        CLUBS("C", "♣"),
        DIAMONDS("D", "♦"),
        HEARTS("H", "♥"),
        SPADES("S", "♠");

        private final String ascii;
        private final String pretty;

        Suit(String ascii, String pretty) {
            this.ascii = ascii;
            this.pretty = pretty;
        }

        public String toAsciiString() {
            return ascii;
        }
        public String toPrettyString() {
            return pretty;
        }
    }

    private final int deckIndex;
    private final Rank rank;
    private final Suit suit;

    public Card(int deckIndex) {
        this.deckIndex = deckIndex;
        rank = Rank.values()[deckIndex / 4];
        suit = Suit.values()[deckIndex % 4];
    }

    public int getDeckIndex() {
        return deckIndex;
    }
    public boolean isRed() {
        return suit == Suit.DIAMONDS || suit == Suit.HEARTS;
    }
    public Rank getRank() {
        return rank;
    }

    public String toAsciiString() {
        return rank.toAsciiString() + suit.toAsciiString();
    }
    public String toPrettyString() {
        return rank.toPrettyString() + " " + suit.toPrettyString();
    }

    public boolean canBePutOnCascade(Card card) {
        return card == null || (card.isRed() != isRed() && card.rank.ordinal() - rank.ordinal() == 1);
    }

    public boolean canBePutOnFoundation(Card card) {
        return card == null ? rank == Rank.ACE : suit == card.suit && rank.ordinal() - card.rank.ordinal() == 1;
    }

}
