# frecl

An attempt at making FreeCell.

Note on terminology: what I call cells, foundations, and cascades are also known as freecells, homecells, and columns.

## Features

- Simple card design.
- Easy keyboard controls.
A solution as a sequence of frecl keypresses is interchangeable with a solution in
[Standard FreeCell Notation](http://www.solitairelaboratory.com/solutioncatalog.html)
(scroll down to "Standard FreeCell Notation (by Andrey Tsouladze)")
by simple character substitution.
- "frecl numbers": a system of numbering every possible deal in lexicographic order.
See [this webpage](http://www.solitairelaboratory.com/fcfaq.html#Supermove)
(scroll down to "How many possible FreeCell deals are there?") for more useful information.
- Compatibility with Microsoft FreeCell deals.
- Logging of completed deal numbers and solutions for future reference and analysis.

## Keyboard controls

(`shift`+)`n`/`m`: (auto-)choose new frecl/Microsoft deal

`a`,`s`,`d`,`f`,`j`,`k`,`l`,`;`: select cascade 1-8

`q`,`w`,`e`,`r`: select cell 1-4

`u`,`i`,`o`,`p`: select foundation 1-4

`space`: move to available empty cell or column

`enter` or `h`: move to *suit*able foundation

`z`: undo

`x`: restart

`/`: input keypresses

## Run

Run `FreclApplication.main()` (or `Main.main()`) or do `mvn javafx:run`.

## Package

Do `mvn package`,
then open file `target/frecl-1.0-SNAPSHOT.jar` or
do `java -jar target/frecl-1.0-SNAPSHOT.jar`.

## `computeFreclNum`

The `Deal` class has a `main` function which computes the frecl number of deal input.
For example, input
```
0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51
```
gives output
```
1
```
and input
```
51,50,49,48,47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,09,08,07,06,05,04,03,02,01,00
```
gives output
```
80658175170943878571660636856403766975289505440883277824000000000000
```
To use, run `Deal.main()`, or after packaging, do `java -cp target/frecl-1.0-SNAPSHOT.jar frecl.frecl.Deal`.
